<?php
include 'webFunctions.php';
session_start();

$userName = $_POST['userName'];
$password = $_POST['password'];
$passwordConfirm = $_POST['confirmPassword'];
$newPassword = $_POST['newPassword'];
$newPasswordConfirm = $_POST['confirmNewPassword'];
if ($_POST['userName'] == "" || $_POST['password'] == "" || $_POST['confirmPassword'] == ""|| $_POST['newPassword'] == "" || $_POST['confirmNewPassword'] == "")
{
    $_SESSION['error'] = 'Not all fields were filled in, please try again.';
    header("Location: createAccountHtml.php");
    exit();
}
else if ($password != $passwordConfirm)
{
    $_SESSION['error'] = 'Current passwords do not match, please try again.';
    header("Location: createAccountHtml.php");
    exit();
}
else if ($newPassword != $newPasswordConfirm)
{
    $_SESSION['error'] = 'New passwords do not match, please try again.';
    header("Location: createAccountHtml.php");
    exit();
}
else
{
    $out = changePassword($pdo, $userName, $password, $newPassword);
    $bool = confirmLogin($pdo, $userName, $password);
    session_regenerate_id (true);
    $_SESSION['key'] = $out['userID'];
    header("Location: homePage.php");
}
?>