<?php
include 'webFunctions.php';
session_start();

$userName = $_POST['userName'];
$email = $_POST['email'];
$password = $_POST['password'];
$passwordConfirm = $_POST['confirmPassword'];
if(isset($_POST['isAdmin']) && $_POST['isAdmin'] == 'Yes') {
	$isAdmin = 1;
}
else {
	$isAdmin = 0;
}
if ($_POST['userName'] == "" || $_POST['email'] == "" || $_POST['password'] == "" || $_POST['confirmPassword'] == "")
{
    $_SESSION['error'] = 'Not all fields were filled in, please try again.';
    header("Location: createAccountHtml.php");
    exit();
}
else if ($password != $passwordConfirm)
{
    $_SESSION['error'] = 'Passwords do not match, please try again.';
    header("Location: createAccountHtml.php");
    exit();
}
else if (! filter_var($email, FILTER_VALIDATE_EMAIL))
{
    $_SESSION['error'] = "$email is not a valid email address, please try again.";
    header("Location: createAccountHtml.php");
    exit();
}
else if (userExists($pdo, $email))
{
    $_SESSION['error'] = "An account has already been created with this email, please try again.";
    header("Location: createAccountHtml.php");
    exit();
}
else
{
    $out = createAccount($pdo, $userName, $email, $password, $isAdmin);
    $bool = confirmLogin($pdo, $userName, $password);
    session_regenerate_id (true);
    $_SESSION['key'] = $out['userID'];
    header("Location: homePage.php");
}
?>