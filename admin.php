<?php
include "header.php";
?>

<?php
if(!isset($_SESSION['key']))
{
    $_SESSION['error'] = 'You must be logged in to view attendance.';
    header("Location: main.php");
    exit();
}
?>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Return home" onclick="window.location.href='homePage.php'" /> <br><br>
<h1>Admin Controls:</h1> 
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Create Account" onclick="window.location.href='createAccountHtml.php'">
<br><br><br>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Change Password" onclick="window.location.href='changePasswordHtml.php'">
<br><br><br>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Export Data to Excel" onclick="window.location.href='exportExcel.php'">

<?php
include "footer.php";
?>