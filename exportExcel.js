function totalsSheet () {
	var times = ["8:30","9:30","10:30","11:30","12:30","13:30","14:30","15:30","16:30","17:30","18:30","19:30","20:30","21:30","22:30","23:30"];

	var row = 0;
	var data = [[]];
	data[row++].push("Totals","Overall","Group--Collab.","Group--Table","Classrooms","Collab as Collab","Collab as Table","Standing Around","Room 137 afterhours");
	for (idx in times) {
		data[row] = [];
		data[row++].push(times[idx]);
	}
	data[row] = [];
	data[row++].push("Totals");
	data[row] = [];
	data[row++].push("Total Check");
	data[row] = [];
	data[row++].push("","","","","","Collab Total");
	data[row] = [];
	data[row].push("","","Group totals");
	var ws = XLSX.utils.aoa_to_sheet(data);
	
	ws['!ref'] = XLSX.utils.encode_range({
		s: { c: 0, r: 0 },
		e: { c: 9, r: 21}
	});
	for (var i = 2; i < 18; i++) {
		ws['B'+i] = { f: 'SUM(MainLEFT!AP'+i+',MainRIGHT!AK'+i+',BottomTop!AQ'+i+')' };
		ws['C'+i] = { f: 'SUM(MainRIGHT!U'+i+',BottomTop!E'+i+',BottomTop!G'+i+')' };
		ws['D'+i] = { f: 'SUM(MainRIGHT!V'+i+',BottomTop!F'+i+',BottomTop!H'+i+')' };
		ws['E'+i] = { f: 'SUM(MainRIGHT!W'+i+',BottomTop!D'+i+')' };
		ws['F'+i] = { f: 'SUM(MainRIGHT!G'+i+',MainRIGHT!I'+i+',MainRIGHT!K'+i+',MainRIGHT!U'+i+',BottomTop!N'+i+',BottomTop!P'+i+')' };
		ws['G'+i] = { f: 'SUM(MainRIGHT!H'+i+',MainRIGHT!J'+i+',MainRIGHT!L'+i+',MainRIGHT!V'+i+',BottomTop!O'+i+',BottomTop!Q'+i+')' };
		ws['H'+i] = { f: 'SUM(MainLEFT!H2'+i+',MainRIGHT!AH'+i+',BottomTop!AM'+i+',BottomTop!AN'+i+')' };
	}
	ws['J2'] = { f: 'SUM(MainRIGHT!U11:U17)' };
	ws['J3'] = { f: 'SUM(MainRIGHT!V11:V17)' };
	ws['J4'] = { f: 'SUM(J2:J3)' };
	
	var clmns = ['B','C','D','E','F','G','H'];
	for (idx in clmns) {
		var clmn = clmns[idx];
		ws[clmn+'18'] = { f: 'SUM('+clmn+'2:'+clmn+'17)' };
	}
	ws['B19'] = { f: 'SUM(MainLEFT!AP18,MainRIGHT!AK18,BottomTop!AQ18)' };
	ws['D21'] = { f: 'SUM(C18,D18,F7)' };
	ws['D22'] = { f: 'SUM(C2:C17,D2:D17)' };
	ws['F21'] = { f: 'SUM(F18,G18)' };
	ws['I2'] = { v: 'Collaboration' };
	ws['I3'] = { v: 'Table' };
	ws['I4'] = { v: 'Total' };
	var style = [];
	style.push({"wch":"10"});
	for(var i = 0; i < 7; i++) {
		style.push({"wch":"15"});
	}
	style.push({"wch":"19"},{"wch":"4"});
	ws['!cols'] = style;
	return ws;
}