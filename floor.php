<?php
include "header.php";

if(!isset($_SESSION['key']))
{
    $_SESSION['error'] = 'You must be logged in to take attendance.';
    header("Location: main.php");
    exit();
}
$floorID = $_GET['floorID'];
$time = $_GET['time'];
$floors = getFloorInfo($pdo, $floorID);
?>
<h2> <?php echo $floors[0]['floorName'];?> </h2>

<div>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Return home" onclick="window.location.href='homePage.php'" />
<form action='submitAttendance.php?floorID=<?php echo $floorID?>&time=<?php echo $time?>' method='post'>
<div class="btn-group">
	<input type = 'submit' style = "padding: 8px 16px;color: white; background-color: black; border-right: none;float: left" name = 'Submit' value = 'Submit'>
</div>
	<?php 
	$sections = getCoords($pdo, $floorID);
	foreach ($sections as $sectionCoords)
	{
	    ?>
		<select required name="section<?php echo $sectionCoords['sectionID']?>" class = "attendance" style = "  border: 3px solid magenta; position: absolute; top:<?php echo $sectionCoords['yLeft']+280?>; left: <?php echo $sectionCoords['xRight']-35?>;">
		<option disabled selected value>Select:</option>
		<?php
    	for ($i = 0; $i <= 8; $i++)
    	{
	    ?>
	    	<option value="<?php echo $i ?>"><?php echo $i; ?></option>
	    	<?php
	    }
	    ?>
	    </select>
	    <?php
	}
	?>

</form>
</div>
<br>
<img alt="Main Floor - left side layout display" src="<?php echo $floors[0]["floorImage"];?>" border = "5">

<?php
include "footer.php";
?>
