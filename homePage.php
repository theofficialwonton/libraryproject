<?php
include "header.php";
?>
<html>
<?php
if(!isset($_SESSION['key']))
{
    $_SESSION['error'] = 'You must be logged in to take attendance.';
    header("Location: main.php");
    exit();
}
date_default_timezone_set('America/New_York');
$now = idate('H');
?>
<h2>
<form action="#" method = "POST">
Select the time slot for which you are taking attendance:
<select name = "formTime" class = "form" style = "  border: 3px solid magenta; position: relative;">
<?php
	for($i = 8; $i < 24; $i++) {
?>
<option value = "<?php echo $i ?>" <?php if(($i == 8 && $now < 9)||($now >= $i && $now < $i + 1)) {echo "selected=selected"; } ?>><?php echo $i ?>:30</option>
<?php
	}
?>
</select>
<input type="submit" style = "padding: 8px 16px;color: white; background-color: black; border-right: none;" value="Confirm Selection"/>
</form>
<?php
if(isset($_POST['formTime'])){
    $time = $_POST['formTime'];
}
?>
</h2>
<h2> Select the section you need:</h2>
<form>
<?php 
$floors = getAllFloors($pdo);
foreach ($floors as $allFloors)
{
?>
		<input type="button" style = "padding: 8px 16px;color: white; background-color: black; border-right: none;float: left" value="<?php echo $allFloors['floorName']?>" onclick="window.location.href='floor.php?floorID=<?php echo $allFloors['floorID']?>&time=<?php echo $time ?>'" /> 
	<?php 
}
?>
</form>
<br>
<?php
if(isset($_SESSION['attendance']))
{
    echo '<h2>'.$_SESSION['attendance'].'</h2>';
    unset($_SESSION['attendance']);
}

include "footer.php";
?>