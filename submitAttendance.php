<?php 
include "header.php";
date_default_timezone_set('America/New_York');
$date = date("Y-m-d");
$time = $_GET['time'];
$timeTaken = $time.":30:00";
$floorID = $_GET['floorID'];
$sections = getCoords($pdo, $floorID);
foreach ($sections as $sectionCoords)
{
    $sectionID = $sectionCoords['sectionID'];
    $numPeople = $_POST['section'.$sectionID];

    takeAttendance($pdo, $numPeople, $sectionID, $floorID, $timeTaken, $date);
}
$floorInfo = getFloorInfo($pdo, $floorID);
$_SESSION['attendance'] = "Attendance for the ".$floorInfo[0]['floorName']. " floor successfully submitted!";
header("Location: homePage.php");
include "footer.php";
?>