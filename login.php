<?php

function userLogin()
{
    include 'webFunctions.php';
    session_start();
    
    if ($_POST['userName'] == "")
    {
        $_SESSION['error'] = 'Please enter your user name.';
        header("Location: main.php");
        exit();
    }
    else if ($_POST['password'] == "")
    {
        $_SESSION['error'] = 'Please enter your password.';
        header("Location: main.php");
        exit();
    }
    $userName = trim($_POST['userName']);
    $password = trim($_POST['password']);
    if (confirmLogin($pdo, $userName, $password))
    {
        session_regenerate_id (true);
        $sql = 'SELECT email, userID FROM users WHERE userName = :userName';
        $s=$pdo ->prepare($sql);
        $s->bindValue(':userName', $userName);
        $s->execute();
        $out = $s->fetch();
        $_SESSION['key'] = $out['userID'];
        echo 'Congrats, you logged in!';
        header("Location:homePage.php");
    }
    else
    {
        $_SESSION['error'] = 'Invalid user name or password';
        header("Location: main.php");
    }
}
userLogin();
?>